<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MenuBuilder
{
	protected $factory;
	protected $authorizationChecker;
	protected $tokenStorage;

	/**
	 * @param FactoryInterface $factory
	 * @param AuthorizationCheckerInterface $authorizationChecker
	 * @param TokenStorageInterface $tokenStorage
	 */
	public function __construct(FactoryInterface $factory, AuthorizationCheckerInterface $authorizationChecker, TokenStorageInterface $tokenStorage)
	{
		$this->factory = $factory;
		$this->authorizationChecker = $authorizationChecker;
		$this->tokenStorage = $tokenStorage;
	}

	public function createMainMenu(array $options)
	{
		$menu = $this->factory->createItem('root');
		$menu->setChildrenAttribute('class', 'nav navbar-nav');

		$menu->addChild('menu.homepage', array('route' => 'homepage'))
			->setExtra('icon', 'fa fa-home')
		;

		if( $this->authorizationChecker->isGranted('ROLE_ADMIN') )
		{
			$menu->addChild('menu.admin', array('route' => 'admin_index'))
				->setExtra('icon', 'fa fa-lock')
			;
		}

		return $menu;
	}

	public function createUserMenu(array $options)
	{
		$menu = $this->factory->createItem('root');
		$menu->setChildrenAttribute('class', 'nav navbar-nav navbar-right');

		if( $this->authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED') )
		{
			$user = $this->tokenStorage->getToken()->getUser();
			$user_menu = $menu->addChild($user->getUsername())
				->setExtra('trans_label', false)
			;

			$user_menu->addChild('menu.profile', array('route' => 'fos_user_profile_edit'))
				->setExtra('icon', 'fa fa-user')
			;
			$user_menu->addChild('menu.change_password', array('route' => 'fos_user_change_password'))
				->setExtra('icon', 'fa fa-key')
			;
			$user_menu->addChild('menu.logout', array('route' => 'fos_user_security_logout'))
				->setAttribute('divider_prepend', true)
				->setExtra('icon', 'fa fa-sign-out')
			;
		}
		else
		{
			$menu->addChild('menu.login', array('route' => 'fos_user_security_login'))
				->setExtra('icon', 'fa fa-sign-in')
			;
		}

		return $menu;
	}

	public function createAdminMenu(array $options)
	{
		$menu = $this->factory->createItem('root');
		$menu->setChildrenAttribute('class', 'nav navbar-nav');

		$menu->addChild('menu.back_to_homepage', array('route' => 'homepage'))
			->setExtra('icon', 'fa fa-home')
		;

		return $menu;
	}
}
